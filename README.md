### How do I get set up? ###

* Summary of set up
* install Java 8 and mysql. Follow these tutorials https://java.com/en/download/help/index_installing.xml and https://dev.mysql.com/usingmysql/get_started.html
* run the following commands in a command promt
* gradlew.bat update
* gradlew.bat war
* gradlew.bat jettyRun (or something elese)

### How to use? ###
* This is the homepage
![2.PNG](https://bitbucket.org/repo/RLrkxR/images/864231042-2.PNG)
* The login button takes you to the login page, where you may login as admin, nuarePAROLA1#@ ; as user, same pass
![3.PNG](https://bitbucket.org/repo/RLrkxR/images/482197923-3.PNG)
* This is the view of the admin. Select one of the tabs to do some operations.
![4.PNG](https://bitbucket.org/repo/RLrkxR/images/815536509-4.PNG)
* Edit your own data.
![6.PNG](https://bitbucket.org/repo/RLrkxR/images/795125632-6.PNG)

* View, remove or edit users.
![7.PNG](https://bitbucket.org/repo/RLrkxR/images/1206293203-7.PNG)