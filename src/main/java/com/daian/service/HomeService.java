package com.daian.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.daian.db.Account;
import com.daian.db.Client;
import com.daian.db.History;
import com.daian.db.Report;
import com.daian.db.User;
import com.daian.db.UserRole;
import com.daian.db.service.DatabaseService;
import com.daian.db.service.DbActivator;
import com.daian.db.service.DbOperation;
import com.daian.security.CustomUserDetail;

@Service
public class HomeService {

	static Logger log = Logger.getLogger(CustomUserDetail.class.getName());
	@Autowired
	private DatabaseService dbService;

	public void editUser(String pass, String name, String sur, String email, String address, String city, String state)
			throws Exception {

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		User user = ((CustomUserDetail) userDetails).getUser();

		// do this to check for errors
		User changes = new User(user.getUsername(), pass, name, sur, user.getNumeric_personal_number(), address, city,
				state, email, user.getEnabled());
		changes.nullCheck();

		dbService.dbFactory.updateObject(user, (DbOperation<User>) (t -> {
			t.setName(name);
			t.setAddress(address);
			t.setCity(city);
			t.setEmail(email);
			t.setPassword(pass);
			t.setState(state);
			t.setSurname(sur);
		}));
	}

	public List<User> findBy(int start, int count, String user, String name, String surname, String city, String state,
			String email, String cnp, Class<?> cls) throws Exception {
		String query = "WHERE ";
		if (name != null) {
			query += "name=:name AND ";
		}
		if (user != null) {
			query += "username=:username AND ";
		}
		if (surname != null) {
			query += "surname=:surname AND ";
		}
		if (city != null) {
			query += "city=:city AND ";
		}
		if (state != null) {
			query += "state=:state AND ";
		}
		if (email != null) {
			query += "email=:email AND ";
		}
		if (cnp != null) {
			query += "numeric_personal_number=:cnp AND ";
		}
		if ("WHERE ".equals(query)) {
			query = "";
		} else {
			query = query.substring(0, query.length() - 4);
		}
		return dbService.dbFactory.getList(start, count, "*", query, t -> {
			if (name != null) {
				t.setString("name", name);
			}
			if (user != null) {
				t.setString("username", user);
			}
			if (surname != null) {
				t.setString("surname", surname);
			}
			if (city != null) {
				t.setString("city", city);
			}
			if (state != null) {
				t.setString("state", state);
			}
			if (email != null) {
				t.setString("email", email);
			}
			if (cnp != null) {
				t.setString("cnp", cnp);
			}
		}, cls);
	}

	public List<History> findHistory(int start, int count, Date date_start, Date date_end) throws Exception {
		return dbService.dbFactory.getList(start, count, "*", "WHERE date>:date_start AND date<:date_end", t -> {
			t.setDate("date_start", date_start);
			t.setDate("date_end", date_end);
		}, History.class);
	}

	public String findReport(String username, int start, int count, Date date_start, Date date_end)
			throws Exception {
		User usr = findBy(0, 1, username, null, null, null, null, null, null, User.class).iterator().next();
		log.debug(username);
		return dbService.dbFactory.getString(start, count, "*",
				"WHERE user=:user AND date>=:date_start AND date<=:date_end",
				t -> {
					t.setDate("date_start", date_start);
					t.setDate("date_end", date_end);
					t.setString("user", username);
				}, Report.class);
	}

	public DatabaseService getDbService() {
		return dbService;
	}

	public void makeClient(String name, String sur, String cnp, String address, String city, String state, String email)
			throws Exception {
		Client userObj = new Client(name, sur, city, state, cnp, address, email);
		userObj.nullCheck();
		// these throw if they dont find
		boolean duplicate = true;
		try {
			dbService.dbFactory.get("*", "WHERE numeric_personal_number = :nr", t -> t.setString("nr", cnp),
					Client.class);
		} catch (Exception e) {
			duplicate = false;
		}
		if (duplicate) {
			throw new Exception("Numeric number is already in use.");
		}
		dbService.dbFactory.addObject(userObj);

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = ((CustomUserDetail) userDetails).getUser();
		Report repo = new Report(userObj.getName(), new Date(), user.getUsername(), "create");
		dbService.dbFactory.addObject(repo);
	}

	public void makeUser(String user, String pass, String name, String sur, String cnp, String address, String city,
			String state, String email) throws Exception {
		User userObj = new User(user, pass, name, sur, cnp, address, city, state, email, 1);
		userObj.nullCheck();
		// these throw if they dont find
		boolean duplicate = true;
		try {
			dbService.dbFactory.get("*", "WHERE username = :username", t -> t.setString("username", user), User.class);
			dbService.dbFactory.get("*", "WHERE numeric_personal_number = :nr", t -> t.setString("nr", cnp),
					User.class);
		} catch (Exception e) {
			duplicate = false;
		}
		if (duplicate) {
			throw new Exception("Username or Numeric number is already in use.");
		}
		dbService.dbFactory.addObject(userObj);
		dbService.dbFactory.addObject(new UserRole(user, "USER"));
	}

	public void removeData(int id, Class<?> cls) throws Exception {
		// also delete roles if user
		Object usr = dbService.dbFactory.get("*", "WHERE id=:id", t -> t.setInteger("id", id), cls);
		if (cls.equals(User.class)) {
			List<UserRole> roles = dbService.dbFactory.getList(0, 2, "*", "WHERE username=:username",
					t -> t.setString("username", ((User)usr).getUsername()), UserRole.class);
			dbService.dbFactory.deleteObjects(roles);
		}
		// also delete accounts if client and history
		if (cls.equals(Client.class)) {
			
			List<Account> accounts = dbService.dbFactory.getList(0, 2, "*", "WHERE client.id=:id",
					t -> t.setInteger("id", id), Account.class);
			dbService.dbFactory.deleteObjects(accounts);
		}
		dbService.dbFactory.deleteObject(usr);
		if (cls.equals(Client.class)) {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			User user = ((CustomUserDetail) userDetails).getUser();
			dbService.dbFactory.addObject(new Report(((Client)usr).getName(), new Date(), user.getUsername(), "delete"));
		}
	}

	public void sendMoney(String from, int ammount, String to) throws Exception {
		Account fromAcc;
		try {
			fromAcc = (Account) dbService.dbFactory.get("*", "WHERE card_number = :cardNr",
					t -> t.setString("cardNr", from), Account.class);
		} catch (Exception e) {
			throw new Exception("Sender account not found.");
		}
		Account toAcc;
		try {
			toAcc = (Account) dbService.dbFactory.get("*", "WHERE card_number = :cardNr",
					t -> t.setString("cardNr", to), Account.class);
		} catch (Exception e) {
			throw new Exception("Receiver account not found.");
		}
		if (ammount < 10 || ammount > 2000) {
			throw new Exception("Invalid ammount.");
		}
		if (fromAcc.getAmmount() - ammount < 0) {
			throw new Exception("Not enought funds. You only have " + fromAcc.getAmmount());
		}

		dbService.dbFactory.updateObject(toAcc, (DbOperation<Account>) t -> t.setAmmount(t.getAmmount() + ammount));
		dbService.dbFactory.updateObject(fromAcc, (DbOperation<Account>) t -> t.setAmmount(t.getAmmount() - ammount));
		History hist = new History();
		hist.setAmmount(ammount);
		hist.setDate(new Date());
		hist.setReceiver(toAcc.getCard_number());
		hist.setSender(fromAcc.getCard_number());
		dbService.dbFactory.addObject(hist);
	}

	public void setDbService(DatabaseService dbService) {
		this.dbService = dbService;
	}

	public void updateClient(int id, String name, String sur, String address, String city, String state, String email)
			throws Exception {
		Client foundObj = dbService.dbFactory.get("*", "WHERE id=:id", t -> t.setInteger("id", id), Client.class);
		Client userObj = new Client(name, sur, city, state, foundObj.getNumeric_personal_number(), address, email);
		userObj.nullCheck();
		dbService.dbFactory.updateObject(foundObj, (DbOperation<Client>) t -> {
			t.setName(name);
			t.setAddress(address);
			t.setCity(city);
			t.setEmail(email);
			t.setState(state);
			t.setSurname(sur);
		});

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = ((CustomUserDetail) userDetails).getUser();
		dbService.dbFactory.addObject(new Report(foundObj.getName(), new Date(), user.getUsername(), "update"));
	}

	public void updateUser(String username, String name, String sur, String address, String city, String state,
			String email) throws Exception {
		User foundObj = dbService.dbFactory.get("*", "WHERE username=:username", t -> t.setString("username", username),
				User.class);
		User userObj = new User(foundObj.getUsername(), foundObj.getPassword(), name, sur,
				foundObj.getNumeric_personal_number(), address, city, state, email, 1);
		userObj.nullCheck();
		dbService.dbFactory.updateObject(foundObj, (DbOperation<User>) t -> {
			t.setName(name);
			t.setAddress(address);
			t.setCity(city);
			t.setEmail(email);
			t.setState(state);
			t.setSurname(sur);
		});
	}

	public String findAccounts(int st, int cnt, int id) throws Exception {
		Client foundObj = dbService.dbFactory.get("*", "WHERE id=:id", t -> t.setInteger("id", id),
				Client.class);
		foundObj.nullCheck();
		return dbService.dbFactory.getString(st, cnt, "*",
				"WHERE client.id=:id",
				t -> {
					t.setInteger("id", id);
				}, Account.class);
	}

	public void makeAccount(int id, String card_num, String acc_type) throws Exception {
		Client client = dbService.dbFactory.get("*", "WHERE id=:id", t -> t.setInteger("id", id),
				Client.class);
		Account acc = new Account(acc_type, 10, client, card_num);
		
		acc.nullCheck();
		// these throw if they dont find
		boolean duplicate = true;
		try {
			dbService.dbFactory.get("*", "WHERE card_number=:nr", t -> t.setString("nr", card_num), Account.class);
		} catch (Exception e) {
			duplicate = false;
		}
		if (duplicate) {
			throw new Exception("Card number is already in use. Internal error occured.");
		}
		dbService.dbFactory.addObject(acc);
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = ((CustomUserDetail) userDetails).getUser();
		Report repo = new Report(client.getName(), new Date(), user.getUsername(), "open_account");
		dbService.dbFactory.addObject(repo);
	}
	
	public void deleteAccount(String card_num) throws Exception {
		Client cl = dbService.dbFactory.get("client", "WHERE card_number=:card_num", t -> t.setString("card_num", card_num),
				Account.class);
		Account acc = dbService.dbFactory.get("*", "WHERE card_number=:card_num", t -> t.setString("card_num", card_num),
				Account.class);
		//dbService.dbFactory.executeInSession((DbOperation<Client>)t -> t.getAccounts().remove(acc), cl);
		dbService.dbFactory.deleteObject(acc);
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		User user = ((CustomUserDetail) userDetails).getUser();
		Report repo = new Report(cl.getName(), new Date(), user.getUsername(), "close_account");
		dbService.dbFactory.addObject(repo);
	}
}