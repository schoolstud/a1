package com.daian.web;

import java.util.Date;
import java.util.List;
import java.util.Random;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.daian.db.Account;
import com.daian.db.Client;
import com.daian.db.History;
import com.daian.db.Report;
import com.daian.db.User;
import com.daian.security.CustomUserDetail;
import com.daian.service.HomeService;

@Controller
public class HomeController {

	static Logger log = Logger.getLogger(HomeController.class.getName());
	private final HomeService homeService;
	Random rand = new Random();
	
	@Autowired
	public HomeController(HomeService helloWorldService) {
		this.homeService = helloWorldService;
	}

	// for 403 access denied page
	@RequestMapping(value = "/403")
	public ModelAndView accesssDenied() {

		ModelAndView model = new ModelAndView();

		// check if user is login
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (!(auth instanceof AnonymousAuthenticationToken) && auth != null) {
			UserDetails userDetail = (UserDetails) auth.getPrincipal();
			model.addObject("username", userDetail.getUsername());
		}

		model.setViewName("403");
		return model;

	}

	@RequestMapping(value = { "/", "/welcome**" }, method = RequestMethod.GET)
	public ModelAndView defaultPage() {

		ModelAndView model = new ModelAndView();

		model.setViewName("index");
		return model;

	}
	
	@RequestMapping(value = "/getAccountClient", method = RequestMethod.GET)
	@ResponseBody
	public String getAccountClient(@RequestParam(value = "id", required = false) String id,
			@RequestParam(value = "pageStart", required = true) String pageStart,
			@RequestParam(value = "pageCount", required = true) String pageCount) {
		String ret = "";

		try {
			ret = homeService.findAccounts(Integer.parseInt(pageStart), Integer.parseInt(pageCount), Integer.parseInt(id));
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/getDataClient", method = RequestMethod.GET)
	@ResponseBody
	public String getDataClient(@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "surname", required = false) String surname,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "state", required = false) String state,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "cnp", required = false) String cnp,
			@RequestParam(value = "pageStart", required = true) String pageStart,
			@RequestParam(value = "pageCount", required = true) String pageCount) {
		String ret = "";

		try {
			List<User> user = homeService.findBy(Integer.parseInt(pageStart), Integer.parseInt(pageCount), null, name,
					surname, city, state, email, cnp, Client.class);
			ret = user.toString();
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/getCurrentData", method = RequestMethod.GET)
	@ResponseBody
	public String getDataUser() {
		String ret = "";

		try {
			UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication()
					.getPrincipal();
			User user = ((CustomUserDetail) userDetails).getUser();
			List<User> users = homeService.findBy(0, 1, user.getUsername(), user.getName(), user.getSurname(),
					user.getCity(), user.getState(), user.getEmail(), user.getNumeric_personal_number(), User.class);
			ret = users.toString();
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/getDataUser", method = RequestMethod.GET)
	@ResponseBody
	public String getDataUser(@RequestParam(value = "username", required = false) String username,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "surname", required = false) String surname,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "state", required = false) String state,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "cnp", required = false) String cnp,
			@RequestParam(value = "pageStart", required = true) String pageStart,
			@RequestParam(value = "pageCount", required = true) String pageCount) {
		String ret = "";

		try {
			log.debug(pageStart);
			log.debug(pageCount);
			log.debug(username);
			List<User> user = homeService.findBy(Integer.parseInt(pageStart), Integer.parseInt(pageCount), username,
					name, surname, city, state, email, cnp, User.class);
			ret = user.toString();
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/getHistory", method = RequestMethod.GET)
	@ResponseBody
	public String getHistory(
			@RequestParam(value = "date_start", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date_start,
			@RequestParam(value = "date_end", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date_end,
			@RequestParam(value = "pageStart", required = true) String pageStart,
			@RequestParam(value = "pageCount", required = true) String pageCount) {
		String ret = "";

		try {
			List<History> user = homeService.findHistory(Integer.parseInt(pageStart), Integer.parseInt(pageCount),
					date_start, date_end);
			ret = user.toString();
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/getReport", method = RequestMethod.GET)
	@ResponseBody
	public String getReport(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "date_start", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date_start,
			@RequestParam(value = "date_end", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") Date date_end,
			@RequestParam(value = "pageStart", required = true) String pageStart,
			@RequestParam(value = "pageCount", required = true) String pageCount) {
		String ret = "";

		log.debug(date_start);

		try {
			String user = homeService.findReport(username, Integer.parseInt(pageStart),
					Integer.parseInt(pageCount), date_start, date_end);
			ret = user;
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/login")
	public ModelAndView login(@RequestParam(value = "error", required = false) String error,
			@RequestParam(value = "logout", required = false) String logout,
			@RequestParam(value = "register", required = false) String register) {

		ModelAndView model = new ModelAndView();
		if (error != null) {
			model.addObject("error", "Invalid username and password!");
		}

		if (register != null) {
			model.addObject("register", "Register an account!");
		}

		model.setViewName("login");

		return model;

	}

	@RequestMapping(value = "/send", method = RequestMethod.POST)
	@ResponseBody
	public String register(@RequestParam(value = "from", required = true) String from,
			@RequestParam(value = "ammount", required = true) String ammount,
			@RequestParam(value = "to", required = true) String to) {

		String ret = "";

		try {
			homeService.sendMoney(from, Integer.parseInt(ammount), to);

		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/editUser", method = RequestMethod.POST)
	@ResponseBody
	public String register(@RequestParam(value = "password", required = true) String pass,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "surname", required = true) String sur,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state) {

		String ret = "";

		try {
			homeService.editUser(pass, name, sur, email, address, city, state);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/deleteAccount", method = RequestMethod.POST)
	@ResponseBody
	public String registerClient(@RequestParam(value = "card_number", required = true) String card_number) {
		String ret = "";
		try {
			homeService.deleteAccount(card_number);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}
	
	@RequestMapping(value = "/registerAccount", method = RequestMethod.POST)
	@ResponseBody
	public String registerClient(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "acc_type", required = true) String acc_type) {
		String ret = "";
		try {
			for(int i = 0; i < 16; i++) {
				ret+=rand.nextInt(10);
			}
			homeService.makeAccount(Integer.parseInt(id), ret, acc_type);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}
	
	@RequestMapping(value = "/registerClient", method = RequestMethod.POST)
	@ResponseBody
	public String registerClient(@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "surname", required = true) String sur,
			@RequestParam(value = "cnp", required = true) String cnp,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state) {
		String ret = "";
		try {
			homeService.makeClient(name, sur, cnp, address, city, state, email);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/registerUser", method = RequestMethod.POST)
	@ResponseBody
	public String registerUser(@RequestParam(value = "user", required = true) String user,
			@RequestParam(value = "password", required = true) String pass,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "surname", required = true) String sur,
			@RequestParam(value = "cnp", required = true) String cnp,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state) {
		String ret = "";
		try {
			homeService.makeUser(user, pass, name, sur, cnp, address, city, state, email);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/removeClient", method = RequestMethod.POST)
	@ResponseBody
	public String removeClient(@RequestParam(value = "id", required = true) String id) {
		String ret = "";
		try {
			homeService.removeData(Integer.parseInt(id), Client.class);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/removeUser", method = RequestMethod.POST)
	@ResponseBody
	public String removeUser(@RequestParam(value = "id", required = true) String id) {
		String ret = "";
		try {
			homeService.removeData(Integer.parseInt(id), User.class);
		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/updateClient", method = RequestMethod.POST)
	@ResponseBody
	public String updateClient(@RequestParam(value = "id", required = true) String id,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "surname", required = true) String sur,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state) {

		String ret = "";

		try {
			homeService.updateClient(Integer.parseInt(id), name, sur, address, city, state, email);

		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}

	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	@ResponseBody
	public String updateUser(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "name", required = true) String name,
			@RequestParam(value = "surname", required = true) String sur,
			@RequestParam(value = "email", required = true) String email,
			@RequestParam(value = "address", required = true) String address,
			@RequestParam(value = "city", required = true) String city,
			@RequestParam(value = "state", required = true) String state) {

		String ret = "";

		try {
			homeService.updateUser(username, name, sur, address, city, state, email);

		} catch (Exception e) {
			ret = e.getMessage();
		}
		return ret;
	}
}