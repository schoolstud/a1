package com.daian.db;

import java.util.Date;

public class Report {
	private String client;
	private Date date;
	private int id;
	private String operation_type;
	private String user;

	public Report() {

	}

	public Report(String client, Date date,  String user, String operation_type) {
		super();
		this.client = client;
		this.date = date;
		this.operation_type = operation_type;
		this.user = user;
	}

	public Report(String client, Date date, int id, String user, String operation_type) {
		super();
		this.client = client;
		this.date = date;
		this.id = id;
		this.operation_type = operation_type;
		this.user = user;
	}

	@Override
	public String toString() {
		return "{\"client\":\"" + client + "\",\"date\":\"" + date + "\",\"id\":\"" + id + "\",\"operation_type\":\""
				+ operation_type + "\",\"user\":\"" + user + "\"}";
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public int getId() {
		return id;
	}

	public String getOperation_type() {
		return operation_type;
	}

	public void nullCheck() throws Exception {
		if (user == null)
			throw new Exception("User is null");
		if (client == null)
			throw new Exception("Client is null");
		if (operation_type == null)
			throw new Exception("Operation type is null");
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setOperation_type(String operation_type) {
		this.operation_type = operation_type;
	}
}
