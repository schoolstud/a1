package com.daian.db;

import java.util.Date;

public class History {
	private int ammount;
	private Date date;
	private int id;
	private String sender, receiver;

	public History() {

	}

	public History(String sender, String receiver, int ammount, Date date) {
		super();
		this.sender = sender;
		this.receiver = receiver;
		this.ammount = ammount;
		this.date = date;
	}

	public int getAmmount() {
		return ammount;
	}

	public Date getDate() {
		return date;
	}

	public int getId() {
		return id;
	}

	public String getReceiver() {
		return receiver;
	}

	public String getSender() {
		return sender;
	}

	public void nullCheck() throws Exception {
		if (sender == null) {
			throw new Exception("Sender account is null");
		}
		if (receiver == null) {
			throw new Exception("Receiver account is null");
		}
		if (ammount < 10 || ammount > 2000) {
			throw new Exception("Ammount is incorect");
		}
		if (date == null) {
			throw new Exception("Date is incorect");
		}
	}

	public void setAmmount(int ammount) {
		this.ammount = ammount;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setReceiver(String receiver) {
		this.receiver = receiver;
	}

	public void setSender(String sender) {
		this.sender = sender;
	}

	@Override
	public String toString() {
		return "{\"ammount\":\"" + ammount + "\",\"date\":\"" + date + "\",\"id\":\"" + id + "\",\"sender\":\""
				+ sender + "\",\"receiver\":\"" + receiver + "\"}";
	}

}
