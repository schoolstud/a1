package com.daian.db;

public class UserRole {
	private int id;
	private String role;
	private String username;

	public UserRole() {

	}

	public UserRole(int id, String username, String role) {
		super();
		this.id = id;
		this.username = username;
		this.role = role;
	}

	public UserRole(String username, String role) {
		super();
		this.username = username;
		this.role = role;
	}

	public int getId() {
		return id;
	}

	public String getRole() {
		return role;
	}

	public String getUsername() {
		return username;
	}

	public void nullCheck() throws Exception {
		if (username == null)
			throw new Exception("Username is null.");
		if (role == null)
			throw new Exception("Role is null.");
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "{\"id\":\"" + id + "\",\"role\":\"" + role + "\",\"username\":\"" + username + "\"}";
	}

}