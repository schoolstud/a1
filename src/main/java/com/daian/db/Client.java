package com.daian.db;

import java.util.HashSet;
import java.util.Set;

public class Client {
	private Set<Account> accounts = new HashSet<Account>();
	private String address;
	private String city;
	private String email;
	private int id;
	private String name;
	private String numeric_personal_number;
	private String state; 
	private String surname;

	public Set<Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Set<Account> accounts) {
		this.accounts = accounts;
	}

	public Client() {
	}

	public Client(String name, String surname, String city, String state, String numeric_personal_number,
			String address, String email) {
		super();
		this.name = name;
		this.surname = surname;
		this.city = city;
		this.state = state;
		this.numeric_personal_number = numeric_personal_number;
		this.address = address;
		this.email = email; 
	}

	public Client(Set<Account> accounts, String address, String city, String email, int id, String name,
			String numeric_personal_number, String state, String surname) {
		super();
		this.accounts = accounts;
		this.address = address;
		this.city = city;
		this.email = email;
		this.id = id;
		this.name = name;
		this.numeric_personal_number = numeric_personal_number;
		this.state = state;
		this.surname = surname;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getEmail() {
		return email;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getNumeric_personal_number() {
		return numeric_personal_number;
	}

	public String getState() {
		return state;
	}

	public String getSurname() {
		return surname;
	}

	public void nullCheck() throws Exception {
		if (name == null)
			throw new Exception("Name is empty");
		if (surname == null)
			throw new Exception("Surname is empty");
		if (city == null)
			throw new Exception("City is empty");
		if (state == null)
			throw new Exception("State is empty");
		if (numeric_personal_number == null)
			throw new Exception("Numeric Personal Code is empty");
		if (address == null)
			throw new Exception("Address is empty");
		if (email == null)
			throw new Exception("Email is empty");
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumeric_personal_number(String numeric_personal_number) {
		this.numeric_personal_number = numeric_personal_number;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Override
	public String toString() {
		return "{\"address\":\"" + address + "\",\"city\":\"" + city + "\",\"email\":\"" + email + "\",\"id\":\"" + id
				+ "\",\"name\":\"" + name + "\",\"numeric_personal_number\":\"" + numeric_personal_number
				+ "\",\"state\":\"" + state + "\",\"surname\":\"" + surname + "\"}";
	}

}