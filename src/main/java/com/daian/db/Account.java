package com.daian.db;

import java.util.HashSet;
import java.util.Set;

public class Account {
	private String acc_type;
	private int ammount;
	private String card_number;
	private Client client;
	private int id;

	public Account() { 
	}

	public Account(String acc_type, int ammount, Client client, String card_number) {
		super();
		this.acc_type = acc_type;
		this.ammount = ammount;
		this.client = client;
		this.card_number = card_number;
	}

	public String getAcc_type() {
		return acc_type;
	}

	public int getAmmount() {
		return ammount;
	}

	public String getCard_number() {
		return card_number;
	}

	public Client getClient() {
		return client;
	}

	public int getId() {
		return id;
	}

	public void nullCheck() throws Exception {
		if (acc_type == null)
			throw new Exception("Account type is empty");
		if (ammount < 10 || ammount > 2000)
			throw new Exception("Ammount is wrong");
		if (client == null)
			throw new Exception("City is empty");
		if (card_number == null)
			throw new Exception("State is empty");
	}

	public void setAcc_type(String acc_type) {
		this.acc_type = acc_type;
	}

	public void setAmmount(int ammount) {
		this.ammount = ammount;
	}

	public void setCard_number(String card_number) {
		this.card_number = card_number;
	}

	public void setClient(Client client) {
		this.client = client;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "{\"acc_type\":\"" + acc_type + "\",\"ammount\":\"" + ammount + "\",\"card_number\":\"" + card_number
				+ "\",\"client_id\":\"" + client.getId() + "\",\"client_name\":\"" + client.getName()+ "\",\"id\":\"" + id + "\"}";
	}

}
