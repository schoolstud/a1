package com.daian.db.service;

import org.hibernate.Query;

@FunctionalInterface
public interface QueryCondition {
	void queryCondition(Query q);
}
