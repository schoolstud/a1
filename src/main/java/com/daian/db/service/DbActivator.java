package com.daian.db.service;

import org.hibernate.Query;

@FunctionalInterface
public interface DbActivator<T>{
	void doOp(T q);
}
