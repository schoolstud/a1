package com.daian.db.service;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.daian.web.HomeController;

@Repository
public class DatabaseService {
	public final DbFactory dbFactory;
	private SessionFactory factory;
	private final Logger logger = LoggerFactory.getLogger(HomeController.class);

	public DatabaseService() {
		try {
			Configuration conf = new Configuration().configure("/hibernate/hibernate.cfg.xml");
			StandardServiceRegistryBuilder ssrb = new StandardServiceRegistryBuilder()
					.applySettings(conf.getProperties());
			factory = conf.buildSessionFactory(ssrb.build());
		} catch (Exception ex) {
			logger.debug(ex.toString());
			ex.printStackTrace();
		}
		dbFactory = new DbFactory(factory);
	}

	public String escape(String value) {
		return value.replace("!", "\\!").replace("%", "\\%").replace("_", "\\_").replace("'", "\\'").replace("\\",
				"\\\\'");
	}
}
