package com.daian.db.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class DbFactory {

	private final SessionFactory factory;

	public DbFactory(SessionFactory factory) {
		this.factory = factory;
	}
	
	public void executeInSession(DbOperation op, Object obj){
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			op.updateObj(obj);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public <T> Integer addObject(T obj) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		Integer objId = null;
		try {
			tx = session.beginTransaction();
			objId = (Integer) session.save(obj);
			tx.commit();
		} catch (Exception e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}
		return objId;
	}

	public <T> void deleteObject(T obj) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			session.delete(obj);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	/**
	 * User one from com.daian.db.
	 * 
	 * @param accs
	 */
	public <T> void deleteObjects(List<T> accs) throws Exception {
		for (T acc : accs) {
			deleteObject(acc);
		} 
	}

	@SuppressWarnings("unchecked")
	public <T> T get(String types, String clause, QueryCondition conds, Class<?> classType) throws Exception {
		Session session = factory.openSession();
		List<Object> objects = null;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Query q = null;

			if (types.equals("*")) {
				q = session.createQuery("FROM " + classType.getName() + " " + clause);
			} else {
				q = session.createQuery("SELECT " + types + " FROM " + classType.getName() + " " + clause);
			}
			q.setFirstResult(0);
			q.setMaxResults(1);

			conds.queryCondition(q);

			objects = q.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}
		if (objects == null) {
			throw new Exception("Data not found");
		} 
		return (T) objects.iterator().next();
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> getList(int startPos, int maxRes, String types, String clause, QueryCondition conds,
			Class<?> classType) throws Exception {
		Session session = factory.openSession();
		List<Object> objects = null;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Query q = null;

			if (types.equals("*")) {
				q = session.createQuery("FROM " + classType.getName() + " " + clause);
			} else {
				q = session.createQuery("SELECT " + types + " FROM " + classType.getName() + " " + clause);
			}
			q.setFirstResult(startPos);
			q.setMaxResults(maxRes);

			conds.queryCondition(q);

			objects = new ArrayList(q.list());
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}
		if (objects == null) {
			throw new Exception("Data not found");
		}
		return (List<T>) objects;
	}

	@SuppressWarnings("unchecked")
	public String getString(int startPos, int maxRes, String types, String clause, QueryCondition conds,
			Class<?> classType) throws Exception {
		String str;
		Session session = factory.openSession();
		List<Object> objects = null;
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Query q = null;

			if (types.equals("*")) {
				q = session.createQuery("FROM " + classType.getName() + " " + clause);
			} else {
				q = session.createQuery("SELECT " + types + " FROM " + classType.getName() + " " + clause);
			}
			q.setFirstResult(startPos);
			q.setMaxResults(maxRes);

			conds.queryCondition(q);

			objects = new ArrayList(q.list());
			str = objects.toString();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}
		if (objects == null) {
			throw new Exception("Data not found");
		}
		return str;
	}
	
	
	public <T> void updateObject(T obj, DbOperation<T> baseOp) throws Exception {
		Session session = factory.openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			baseOp.updateObj(obj);
			session.update(obj);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		} finally {
			session.close();
		}
	}

	public <T> void updateObjects(List<T> objs, final DbOperation<T> op) throws Exception {
		for (T obj : objs) {
			updateObject(obj, op);
		}
	}
}
