package com.daian.db.service;

@FunctionalInterface
public interface DbOperation<T> {
	public void updateObj(T account);
}