package com.daian.db;

import java.util.HashSet;
import java.util.Set;

public class User {
	private String address;
	private String city;
	private String email;
	private int enabled;
	private int id;
	private String name;
	private String numeric_personal_number;
	private String password;
	private String state;
	private String surname;
	private String username;

	public User() {
	}

	public User(String username, String password, String name, String surname, String numeric_personal_number,
			String address, String city, String state, String email, int enabled) {
		super();
		this.username = username;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.numeric_personal_number = numeric_personal_number;
		this.address = address;
		this.enabled = enabled;
		this.city = city;
		this.state = state;
		this.email = email;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getEmail() {
		return email;
	}

	public int getEnabled() {
		return enabled;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getNumeric_personal_number() {
		return numeric_personal_number;
	}

	public String getPassword() {
		return password;
	}

	public String getState() {
		return state;
	}

	public String getSurname() {
		return surname;
	}

	public String getUsername() {
		return username;
	}

	public void nullCheck() throws Exception {
		if (username == null)
			throw new Exception("Username is empty");
		if (password == null)
			throw new Exception("Password is empty");
		if (name.equals(""))
			throw new Exception("Name is empty");
		if (surname == null)
			throw new Exception("Surname is empty");
		if (city == null)
			throw new Exception("City is empty");
		if (state == null)
			throw new Exception("State is empty");
		if (numeric_personal_number == null)
			throw new Exception("Numeric Personal Code is empty");
		if (address == null)
			throw new Exception("Address is empty");
		if (email == null)
			throw new Exception("Email is empty");
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setNumeric_personal_number(String numeric_personal_number) {
		this.numeric_personal_number = numeric_personal_number;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "{\"address\":\"" + address + "\",\"city\":\"" + city + "\",\"email\":\"" + email + "\",\"enabled\":\""
				+ enabled + "\",\"id\":\"" + id + "\",\"name\":\"" + name + "\",\"numeric_personal_number\":\""
				+ numeric_personal_number + "\",\"password\":\"" + password + "\",\"state\":\"" + state
				+ "\",\"surname\":\"" + surname + "\",\"username\":\"" + username + "\"}";
	}

}