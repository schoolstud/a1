package com.daian.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.daian.db.User;
import com.daian.db.UserRole;
import com.daian.db.service.DatabaseService;

@SuppressWarnings("serial")
public class CustomUserDetail implements UserDetails {

	static Logger log = Logger.getLogger(CustomUserDetail.class.getName());
	private final DatabaseService service;
	private final User user;

	public CustomUserDetail(User user, DatabaseService service) {
		log.debug(user + " this is user");
		this.user = user;
		this.service = service;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		List<String> list = null;
		try {
			list = service.dbFactory.getList(0, 2, "role", "WHERE username = :USER",
					t -> t.setString("USER", user.getUsername()), UserRole.class);
		} catch (Exception e) {
			// This should never ever happen
			// if it does we return a new empty list
			e.printStackTrace();
			return new ArrayList<SimpleGrantedAuthority>();
		}
		List<SimpleGrantedAuthority> auth = new ArrayList<SimpleGrantedAuthority>();
		for (String role : list) {
			auth.add(new SimpleGrantedAuthority(role));
		}
		return auth;
	}

	@Override
	public String getPassword() {
		return user.getPassword();
	}

	public User getUser() {
		return user;
	}

	@Override
	public String getUsername() {
		return user.getUsername();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		if (user == null)
			return false;
		return user.getEnabled() == 1 ? true : false;
	}

}
