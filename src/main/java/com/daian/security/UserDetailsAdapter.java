package com.daian.security;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.daian.db.User;
import com.daian.db.service.DatabaseService;

@Service
public class UserDetailsAdapter implements UserDetailsService {
	@Autowired
	private DataSource dataSource;
	@Autowired
	private DatabaseService dbService;

	public DataSource getDataSource() {
		return dataSource;
	}

	public DatabaseService getDbService() {
		return dbService;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = null;
		try {
			user = (User) dbService.dbFactory.get("*", "WHERE username = :username",
					t -> t.setString("username", username), User.class);
		} catch (Exception e) {
			// This should never ever happen
			// if it does, send an null filled user detail as requested by
			// spring
			e.printStackTrace();
		}
		return new CustomUserDetail(user, dbService);
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public void setDbService(DatabaseService dbService) {
		this.dbService = dbService;
	}

}